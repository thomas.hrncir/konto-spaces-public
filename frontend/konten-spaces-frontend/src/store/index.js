import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        allKontoOverview: [],

        allKontoViewAll: [],

        postKontoDialog: "",

        allSpacesByKontoId: [],

        accountBalance: ""
    },

    mutations: {
        setAllKontoOverview(state, allKontos) {
            state.allKontoOverview = allKontos;
        },

        setAllKontoViewAll(state, allKontoViewAll) {
            state.allKontoViewAll = allKontoViewAll
        },

        setAllSpacesByKontoId(state, allSpaces) {
            state.allSpacesByKontoId = allSpaces;
        },

        setAccountBalance(state, accountBalance) {
            state.accountBalance = accountBalance;
        }
    },

    actions: {

        async getAllKontoOveview(context) {
            const response = await Vue.axios.get("/konto/all");
            context.commit('setAllKontoOverview', response.data);
            console.log(response.data)
            this.state.allSpacesByKontoId = []
        },

        async getAllKontoViewAll(context) {
            const response = await Vue.axios.get("konto/getAll");
            context.commit('setAllKontoViewAll', response.data);
            console.log(response.data)
        },

        async postKonto(context, kontoDetails) {
            console.log(kontoDetails)
            const response = await Vue.axios.post("/konto", kontoDetails);
            console.warn(response.status)
            context.dispatch('getAllKontoOveview')
            return response.status
        },

        async getAllSpaceByKontoId(context, kontoId) {
            console.log(kontoId)
            const response = await Vue.axios.get("/konto/getDetail/" + kontoId);
            context.commit('setAllSpacesByKontoId', response.data)
            console.log(response)
        },

        async postSpace(context, spaceDetails) {
            const response = await Vue.axios.post("/space/setSpace", spaceDetails);
            context.dispatch('getAllSpaceByKontoId', spaceDetails.kontoId)
            context.dispatch('getAccountBalance', spaceDetails.kontoId)
            console.log(response.status)
        },

        async getAccountBalance(context, kontoId) {
            const response = await Vue.axios.get("/konto/getDetail/" + kontoId + "/accountBalance");
            context.commit('setAccountBalance', response.data)
        },

        async deleteSpace(context, deleteSpaceDetails) {
            await Vue.axios.delete("/space/delete/spaceId/" + deleteSpaceDetails.spaceId);
            context.dispatch('getAllSpaceByKontoId', deleteSpaceDetails.kontoId)
            context.dispatch('getAccountBalance', deleteSpaceDetails.kontoId)
        },

        async spaceDeposit(context, depositDetails) {
            await Vue.axios.post("/space/deposit", depositDetails);
            context.dispatch('getAllSpaceByKontoId', depositDetails.kontoId)
            context.dispatch('getAccountBalance', depositDetails.kontoId)
        },

        async spaceWithdraw(context, withdrawDetails) {
            await Vue.axios.post("/space/withdraw", withdrawDetails);
            context.dispatch('getAllSpaceByKontoId', withdrawDetails.kontoId)
            context.dispatch('getAccountBalance', withdrawDetails.kontoId)
        },

        async transferFunds(context, transferDetails) {
            await Vue.axios.post('/space/setAccountBalance', transferDetails);
            context.dispatch('getAllKontoOveview')
        }
    }
})