import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/DashBoard.vue'
import KontoDetail from '../views/KontoDetail.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        children: [
            {
                path: 'detail',
                component: KontoDetail,
            }
        ]
    },

    {
        path: '/kontodetail/:kontoId',
        name: 'KontoDetail',
        component: KontoDetail
    },
]

const router = new VueRouter({
    routes
})

export default router