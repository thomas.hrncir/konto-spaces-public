package com.example.kontospaceslearntest;

import com.example.kontospaceslearntest.repository.KontoCRUDRepository;
import com.example.kontospaceslearntest.repository.SpaceCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KontoSpacesLearntestApplication implements CommandLineRunner {

	@Autowired
	KontoCRUDRepository kontoCRUDRepository;

	@Autowired
	SpaceCRUDRepository spaceCRUDRepository;

	public static void main(String[] args) {
		SpringApplication.run(KontoSpacesLearntestApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {


	}

}
