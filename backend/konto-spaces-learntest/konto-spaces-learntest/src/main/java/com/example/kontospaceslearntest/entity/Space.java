package com.example.kontospaceslearntest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

@Entity
public class Space {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private double accountBalance;

    @ManyToOne
    @JsonBackReference(value = "kontoSpace")
    @JoinColumn(name = "konto_id")
    private Konto konto;


    public Space() {
    }

    public Space(String name, String description, double accountBalance, Konto konto) {
        this.name = name;
        this.description = description;
        this.accountBalance = accountBalance;
        this.konto = konto;
    }

    //getter & setter


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Konto getKonto() {
        return konto;
    }

    public void setKonto(Konto konto) {
        this.konto = konto;
    }
}
