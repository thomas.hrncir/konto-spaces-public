package com.example.kontospaceslearntest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
public class Konto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @Column
    private String description;

    @OneToMany
    @JsonManagedReference(value = "kontoSpace")
    private Set<Space> spacesSet = new HashSet<>();

    @Column
    private String color;

    public Konto() {
    }

    public Konto(int id, String name, String description, String color) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.color = color;
    }

    //getter & setter


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Space> getSpacesSet() {
        return spacesSet;
    }

    //adds space objekt to set
    public void setSpacesSet(Space space) {
        this.spacesSet.add(space);
    }

    public void deleteSpaceFromSpacesSet(Space space) {
        this.spacesSet.remove(space);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
