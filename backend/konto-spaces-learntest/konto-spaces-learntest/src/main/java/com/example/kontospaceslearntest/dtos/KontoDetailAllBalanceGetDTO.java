package com.example.kontospaceslearntest.dtos;

public class KontoDetailAllBalanceGetDTO {

    private double allAccountBalance;


    public KontoDetailAllBalanceGetDTO(double allAccountBalance) {
        this.allAccountBalance = allAccountBalance;
    }

    public double getAllAccountBalance() {
        return allAccountBalance;
    }

    public void setAllAccountBalance(double allAccountBalance) {
        this.allAccountBalance = allAccountBalance;
    }
}
