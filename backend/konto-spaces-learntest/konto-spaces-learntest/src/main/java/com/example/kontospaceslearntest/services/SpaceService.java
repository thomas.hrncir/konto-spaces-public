package com.example.kontospaceslearntest.services;

import com.example.kontospaceslearntest.dtos.SpaceTransferAmountDTO;
import com.example.kontospaceslearntest.entity.Konto;
import com.example.kontospaceslearntest.entity.Space;
import com.example.kontospaceslearntest.repository.KontoCRUDRepository;
import com.example.kontospaceslearntest.repository.SpaceCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SpaceService {

    @Autowired
    KontoCRUDRepository kontoCRUDRepository;

    @Autowired
    SpaceCRUDRepository spaceCRUDRepository;

    public Integer postSpace(Space space) {
        return spaceCRUDRepository.save(space).getId();
    }

    public List<Space> getAllSpaces() {
        return (List<Space>) spaceCRUDRepository.findAll();
    }

    public void setSpace(String spaceName, String spaceDescription, Double accountBalance, Integer kontoId) {

        Konto konto = kontoCRUDRepository.findById(kontoId).get();
        Space space = new Space(spaceName, spaceDescription, accountBalance, konto);

        konto.setSpacesSet(space);

        spaceCRUDRepository.save(space);
        kontoCRUDRepository.save(konto);
    }

    public void transferBalanceSpace(int space1_id, Integer space2_id, Double amount) throws Exception {

        Space space1 = spaceCRUDRepository.findById(space1_id).get();
        Space space2 = spaceCRUDRepository.findById(space2_id).get();

        double space1Balance = space1.getAccountBalance();
        double space1NewBalance = space1Balance - amount;
        double space2Balance = space2.getAccountBalance();
        double space2NewBalance;
        if (space1NewBalance < 0) {
//            throw new Exception("Test");
        } else {
            space2NewBalance = space2Balance + amount;
            space1.setAccountBalance(space1NewBalance);
            space2.setAccountBalance(space2NewBalance);
            spaceCRUDRepository.save(space1);
            spaceCRUDRepository.save(space2);

        }
    }

    public void depositBalance(int spaceId, double amount) {
        Space space = spaceCRUDRepository.findById(spaceId).get();

        double spaceBalance = space.getAccountBalance();
        double newSpaceBalance = spaceBalance + amount;

        space.setAccountBalance(newSpaceBalance);
        spaceCRUDRepository.save(space);
    }

    public void withdrawBalance(int spaceId, double amount) {
        Space space = spaceCRUDRepository.findById(spaceId).get();

        double spaceBalance = space.getAccountBalance();
        double newSpaceBalance = spaceBalance - amount;

        if (newSpaceBalance < 0) {

        } else {
            space.setAccountBalance(newSpaceBalance);
            spaceCRUDRepository.save(space);
        }
    }

    public void deleteSpace(int spaceId) {
        Space space = spaceCRUDRepository.findById(spaceId).get();
        Konto konto = space.getKonto();
        konto.deleteSpaceFromSpacesSet(space);
        spaceCRUDRepository.deleteById(spaceId);
    }
}
