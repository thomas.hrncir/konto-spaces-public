package com.example.kontospaceslearntest.dtos;

import com.example.kontospaceslearntest.entity.Space;

import java.util.HashSet;
import java.util.Set;

public class KontoDetailGetDTO {

    Set<Space> spaces = new HashSet<>();
    private int id;
    private String name;
    private String description;

    private String color;


    public KontoDetailGetDTO(Set<Space> spaces, int id, String name, String description, String color) {
        this.spaces = spaces;
        this.id = id;
        this.name = name;
        this.description = description;
        this.color = color;
    }

    //getter & setter

    public Set<Space> getSpaces() {
        return spaces;
    }

    public void setSpaces(Set<Space> spaces) {
        this.spaces = spaces;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
