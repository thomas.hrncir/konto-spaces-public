package com.example.kontospaceslearntest.repository;

import com.example.kontospaceslearntest.entity.Konto;
import com.example.kontospaceslearntest.entity.Space;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpaceCRUDRepository extends CrudRepository<Space, Integer> {
}
