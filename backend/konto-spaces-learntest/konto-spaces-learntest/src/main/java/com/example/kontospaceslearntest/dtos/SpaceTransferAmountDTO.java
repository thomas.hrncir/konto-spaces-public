package com.example.kontospaceslearntest.dtos;

public class SpaceTransferAmountDTO {

    int space1_id;
    int space2_id;
    double transferAmount;

    public SpaceTransferAmountDTO(int space1_id, int space2_id, double transferAmount) {
        this.space1_id = space1_id;
        this.space2_id = space2_id;
        this.transferAmount = transferAmount;
    }

    //getter & setter


    public int getSpace1_id() {
        return space1_id;
    }

    public void setSpace1_id(int space1_id) {
        this.space1_id = space1_id;
    }

    public int getSpace2_id() {
        return space2_id;
    }

    public void setSpace2_id(int space2_id) {
        this.space2_id = space2_id;
    }

    public double getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(double transferAmount) {
        this.transferAmount = transferAmount;
    }
}
