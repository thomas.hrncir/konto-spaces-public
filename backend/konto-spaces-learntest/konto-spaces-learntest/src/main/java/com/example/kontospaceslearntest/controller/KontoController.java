package com.example.kontospaceslearntest.controller;

import com.example.kontospaceslearntest.dtos.KontoDetailAllBalanceGetDTO;
import com.example.kontospaceslearntest.dtos.KontoDetailGetDTO;
import com.example.kontospaceslearntest.dtos.KontoGetDTO;
import com.example.kontospaceslearntest.entity.Konto;
import com.example.kontospaceslearntest.services.KontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class KontoController {

    @Autowired
    KontoService kontoService;

    @CrossOrigin
    @PostMapping("/konto")
    public Integer postKonto(@RequestBody Konto konto) {
        return kontoService.postKonto(konto);
    }

    @CrossOrigin
    @GetMapping("/konto/getAll")
    public List<Konto> getKonto() {
        return kontoService.getAllKontos();
    }

    @CrossOrigin
    @GetMapping("/konto/getDetail/{kontoId}")
    public KontoDetailGetDTO kontoDetailGetDTO(@PathVariable int kontoId) {
        return kontoService.kontoDetailGetDTO(kontoId);
    }

    @CrossOrigin
    @GetMapping("/konto/all")
    public List<KontoGetDTO> kontoGetDTO() {
        return kontoService.kontoGetDTO();
    }

    @CrossOrigin
    @GetMapping("/konto/getDetail/{kontoId}/accountBalance")
    public double kontoDetailAllBalanceGetDTO(@PathVariable int kontoId) {
        return kontoService.allaccount(kontoId);
    }

    @CrossOrigin
    @DeleteMapping("/konto/delete/{kontoId}")
    public void deleteKonto(@PathVariable int kontoId) {
        kontoService.deleteKonto(kontoId);
    }


}
