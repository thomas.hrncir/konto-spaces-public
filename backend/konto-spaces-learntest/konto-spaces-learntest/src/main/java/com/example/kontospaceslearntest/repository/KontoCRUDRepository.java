package com.example.kontospaceslearntest.repository;

import com.example.kontospaceslearntest.entity.Konto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KontoCRUDRepository extends CrudRepository<Konto, Integer> {
}
