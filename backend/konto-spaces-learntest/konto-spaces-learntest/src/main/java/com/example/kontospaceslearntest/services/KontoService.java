package com.example.kontospaceslearntest.services;

import com.example.kontospaceslearntest.dtos.KontoDetailAllBalanceGetDTO;
import com.example.kontospaceslearntest.dtos.KontoDetailGetDTO;
import com.example.kontospaceslearntest.dtos.KontoGetDTO;
import com.example.kontospaceslearntest.entity.Konto;
import com.example.kontospaceslearntest.entity.Space;
import com.example.kontospaceslearntest.repository.KontoCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class KontoService {

    @Autowired
    KontoCRUDRepository kontoCRUDRepository;

    public Integer postKonto(Konto konto) {
        return kontoCRUDRepository.save(konto).getId();
    }

    public List<Konto> getAllKontos() {
        return (List<Konto>) kontoCRUDRepository.findAll();
    }

//    public KontoDetailGetDTO kontoDetailGetDTO(int kontoId) {
//        Optional<Konto> konto = kontoCRUDRepository.findById(kontoId);
//
//    }

    public KontoDetailGetDTO kontoDetailGetDTO(int kontoId) {
        Konto konto = kontoCRUDRepository.findById(kontoId).get();
        return new KontoDetailGetDTO(konto.getSpacesSet(), konto.getId(), konto.getName(), konto.getDescription(), konto.getColor());
    }

    public List<KontoGetDTO> kontoGetDTO() {
        List<KontoGetDTO> kontoGetDTOS = new ArrayList<>();

        kontoCRUDRepository.findAll().forEach(konto -> {
            KontoGetDTO kontoDto;
            kontoDto = new KontoGetDTO(konto.getId(), konto.getName(), konto.getDescription(), konto.getColor());
            kontoGetDTOS.add(kontoDto);
        });

        return kontoGetDTOS;
    }

    public double allaccount(int kontoId) {
        Set<Space> kontoDetailAllBalanceGetDTOS = kontoCRUDRepository.findById(kontoId).get().getSpacesSet();
        double accountBalance = 0;
        for (Space space : kontoDetailAllBalanceGetDTOS) {
            accountBalance += space.getAccountBalance();
        }
        //return new KontoDetailAllBalanceGetDTO(allBalance.get());
        return accountBalance;
    }

    public void deleteKonto(int kontoId) {
        //TODO: delete all Spaces inside Konto before
        kontoCRUDRepository.deleteById(kontoId);
    }


}
