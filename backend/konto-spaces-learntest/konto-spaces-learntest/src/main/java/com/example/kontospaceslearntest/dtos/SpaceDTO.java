package com.example.kontospaceslearntest.dtos;

public class SpaceDTO {

    private String spaceName;
    private String spaceDescription;
    private double accountBalance;
    private int kontoId;

    public SpaceDTO(String spaceName, String spaceDescription, double accountBalance, int kontoId) {
        this.spaceName = spaceName;
        this.spaceDescription = spaceDescription;
        this.accountBalance = accountBalance;
        this.kontoId = kontoId;
    }

    //getter & setter


    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getSpaceDescription() {
        return spaceDescription;
    }

    public void setSpaceDescription(String spaceDescription) {
        this.spaceDescription = spaceDescription;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getKontoId() {
        return kontoId;
    }

    public void setKontoId(int kontoId) {
        this.kontoId = kontoId;
    }
}
