package com.example.kontospaceslearntest.controller;

import com.example.kontospaceslearntest.dtos.SpaceDTO;
import com.example.kontospaceslearntest.dtos.SpaceDepositWithdrawDTO;
import com.example.kontospaceslearntest.dtos.SpaceTransferAmountDTO;
import com.example.kontospaceslearntest.entity.Konto;
import com.example.kontospaceslearntest.entity.Space;
import com.example.kontospaceslearntest.services.SpaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SpaceController {
    @Autowired
    SpaceService spaceService;

    @CrossOrigin
    @PostMapping("/space")
    public Integer postSpace(@RequestBody Space space) {
        return spaceService.postSpace(space);
    }

    @CrossOrigin
    @GetMapping("/space/getAll")
    public List<Space> getAllSpaces() {
        return spaceService.getAllSpaces();
    }

    @CrossOrigin
    @PostMapping("/space/setSpace")
    public void setSpace(@RequestBody SpaceDTO spaceDTO) {
        spaceService.setSpace(spaceDTO.getSpaceName(), spaceDTO.getSpaceDescription(), spaceDTO.getAccountBalance(), spaceDTO.getKontoId());
    }

    @CrossOrigin
    @PostMapping("/space/setAccountBalance")
    public void setAccountBalance(@RequestBody SpaceTransferAmountDTO spaceTransferAmountDTO) throws Exception {
        spaceService.transferBalanceSpace(spaceTransferAmountDTO.getSpace1_id(), spaceTransferAmountDTO.getSpace2_id(), spaceTransferAmountDTO.getTransferAmount());
    }

    @CrossOrigin
    @PostMapping("/space/deposit")
    public void spaceDeposit(@RequestBody SpaceDepositWithdrawDTO spaceDepositWithdrawDTO) {
        spaceService.depositBalance(spaceDepositWithdrawDTO.getSpaceId(), spaceDepositWithdrawDTO.getAmount());
    }

    @CrossOrigin
    @PostMapping("/space/withdraw")
    public void spaceWithdraw(@RequestBody SpaceDepositWithdrawDTO spaceDepositWithdrawDTO) {
        spaceService.withdrawBalance(spaceDepositWithdrawDTO.getSpaceId(), spaceDepositWithdrawDTO.getAmount());
    }

    @CrossOrigin
    @DeleteMapping("/space/delete/spaceId/{spaceId}")
    public void deleteSpace(@PathVariable int spaceId) {
        spaceService.deleteSpace(spaceId);
    }
}
