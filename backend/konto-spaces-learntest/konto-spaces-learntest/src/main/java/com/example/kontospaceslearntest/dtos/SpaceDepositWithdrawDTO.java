package com.example.kontospaceslearntest.dtos;

public class SpaceDepositWithdrawDTO {

    int spaceId;
    double amount;

    public SpaceDepositWithdrawDTO(int spaceId, double amount) {
        this.spaceId = spaceId;
        this.amount = amount;
    }

    //getter & setter


    public int getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(int spaceId) {
        this.spaceId = spaceId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
